const models = require('../models')

const getPrice = (req, res) => {
  const {cut, color, clarity, carat} = req.query
  models.price.findOne({where: {cut, color, clarity}})
  .then((row) => {
    if (row) {
      const price = carat * row.price
      res.json(price)
    } else res.status(400).json({error: `Wasn't able to calculate the price for this diamond`})
  })
  .catch((error) => {
    console.log(error)
    res.status(400).json({error})
  })
}


module.exports = {
  getPrice,
}
