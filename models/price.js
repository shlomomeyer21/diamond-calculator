'use strict'
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('price', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    cut: {
      type: DataTypes.STRING
    },
    color: {
      type: DataTypes.STRING
    },
    clarity: {
      type: DataTypes.STRING
    },
    carat: {
      type: DataTypes.DOUBLE
    },
    price: {
      type: DataTypes.INTEGER
    },
  })
}