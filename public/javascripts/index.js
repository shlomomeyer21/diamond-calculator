const form = document.querySelector('form')
form.addEventListener('submit', (e) => {
  e.preventDefault()
  document.querySelector('.error').innerHTML = ''
  getReq(`/price/?${new URLSearchParams(new FormData(form)).toString()}`)
  .then((result) => {
    document.querySelector('.price').innerHTML = `Price: $${result}.00`
  })
  .catch(((error) => {
    document.querySelector('.error').innerHTML = error
    console.error(error)
  }))
})

const getReq = (path) => {
  return new Promise((resolve, reject) => {
    const req = new XMLHttpRequest()
    req.addEventListener("load", () => {
      handleResponse(req, resolve, reject)
    })
    req.addEventListener("error", (error) => {
      reject(error)
    })
    req.addEventListener("abort", (error) => {
      reject(error)
    })
    req.withCredentials = true
    req.open("GET", path, true)
    req.send()
  })
}


const handleResponse = (req, resolve, reject) => {
  if (req.status >= 400) reject(`Error: Status Code ${req.status}`)
  else resolve(JSON.parse(req.responseText))
}