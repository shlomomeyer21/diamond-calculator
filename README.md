# Diamond Calculator


> ### Example Node (Express + MySQL) codebase containing a fullstack implementation of a diamond calculator. The prices are calculated based on mock data of previous prices for diamonds that all have a weight of 1 carat. Each diamond in the mock data was given a price based on cut, clarity and color with a ratio of 20:15:15 in that order. The weight of the diamond in question is then multiplied by the price of the mock data which as weight of 1 carat. The design of the calculator is a bit funky so beware :)

# Getting started

To get the Node server running locally:

- Clone this repo
- `npm install` to install all required dependencies
- `npm run db:init` to init the database and tables
- `npm run start` to start the local server


## Application Structure

- `bin/www` - The place where we configure and boot app.js from
- `app.js` - The entry point to our application. This file defines our express server and connects it to MongoDB using mongoose. It also requires the routes and models we'll be using in the application.
- `config/` - This folder contains configuration for Sequelize
- `controllers/` - This folder is the C of the MVC pattern. 
- `internals/` - This folder internal stuff such as scripts for internal use etc
- `models/` - This folder is the M of the MVC pattern. It contains the schema definitions for our Sequelize models.
- `node_modules/` - This folder contains all of our npm dependencies
- `public/` - This folder contains all of the public static assets such as css, javascript etc
- `routes/` - This folder contains the route definitions for our API to access our resources.
- `views/` - This folder is the V of the MVC pattern. It contains the views files that will be rendered with the pug engine
- `package.json/` - This file contains all of the npm configuration for this app
