const models = require('../../models')

models.sequelize.sync().then(() => {
  const getPercent = (array, arrayValue) => {
    const position = array.indexOf(arrayValue) + 1
    return (position / array.length) * 100
  }

  const cutArr = ['poor', 'fair', 'good', 'very good', 'excellent', 'ideal']
  const colorArr = ['D', 'E', 'F', 'G', 'H', 'I', 'J']
  const clarityArr = ['IF', 'VVS1', 'VVS2', 'VS1', 'VS2', 'SI1', 'SI2', 'I1', 'I2', 'I3']

  const getPriceForCut = (cut) => getPercent(cutArr, cut) * 20
  const getPriceForColor = (color) => getPercent(colorArr, color) * 15
  const getPriceForClarity = (clarity) => getPercent(clarityArr, clarity) * 15


  const arr = []

  for (let i = 0; i < cutArr.length; i++) {
    for (let j = 0; j < colorArr.length; j++) {
      for (let k = 0; k < clarityArr.length; k++) {
        const price = Math.round(getPriceForCut(cutArr[i]) + getPriceForColor(colorArr[j]) + getPriceForClarity(clarityArr[k]))
        const data = {cut: cutArr[i], color: colorArr[j], clarity: clarityArr[k], carat: 1.00, price}
        arr.push(models.price.create(data))
      }
    }
  }

})
